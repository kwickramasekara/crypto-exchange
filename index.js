var request = require( 'request' );
var rsvp = require( 'rsvp' );
var moment = require( 'moment-timezone' );
var fs = require( 'fs' );

// Data needed for the text message API
var smsData = require( './smsData.json' );

var settings = {
	updateInterval: 30, // minute(s)
	exchangeRateURL: 'https://api.gemini.com/v1/trades/',
	exchangeType: 'ethusd',
	changeTimeDifference: 12, // hour(s),
	triggerPercentage: 5,
	logFile: 'log.txt'
};

var lastNotificationTimestamp = moment().subtract( 1, 'days').format( 'X' );

var saveToLog = function( message, category ) {
	var newline = '\n';
	var logDateFormat = 'MM/DD/YY, h:mm:ss a';
	var data;

	category = category || 'unknown';
	data = moment().format( logDateFormat ) + ' - ' + '[ Category: ' + category + ' ] ' + message + newline;

	fs.appendFile( settings.logFile, data, function( error ) {
		console.log( error );
	});
};

var makeRequest = function( options ) {
	var deferred = rsvp.defer();
	var callback = function( error, response, body ) {
		if ( error !== null ) {
			saveToLog( error, 'HTTP[S] Request' );
			deferred.reject( false );
		}

		deferred.resolve( body );
	};

	request( options, callback );

	return deferred.promise;
};

var getPrice = function( options ) {
	var deferred = rsvp.defer();

	makeRequest( options ).then( function( response ) {
		response = response ? JSON.parse( response ) : '';

		if ( response.result === 'error' ) {
			saveToLog( response.message, 'API Request' );
			deferred.reject( false );
		}

		deferred.resolve( parseFloat( response[0].price ) );
	});

	return deferred.promise;
};

var calculateValues = function( currentPrice, historicalPrice ) {
	var priceDifference = parseFloat( ( currentPrice - historicalPrice ).toFixed( 2 ) );
	var percentageDifference = parseFloat( ( ( priceDifference / historicalPrice ) * 100 ).toFixed( 2 ) );
	var absolutePercentageDifference = Math.abs( percentageDifference );

	return {
		currentPrice: currentPrice,
		historicalPrice: historicalPrice,
		priceDifference: priceDifference,
		percentageDifference: percentageDifference,
		absolutePercentageDifference: absolutePercentageDifference
	};
};

var isConditionMet = function( values, triggerPercentage ) {
	if ( values.absolutePercentageDifference >= triggerPercentage ) {
		return true;
	}

	return false;
};

var generateMessage = function( values, type, changeTimeDifference ) {
	var devider = ' / ';

	return 'Cryptocurrency alert(' + type + ')\n' + 'Current: ' + values.currentPrice + devider + changeTimeDifference + 'h ago: ' + values.historicalPrice + devider + 'Change: ' + values.percentageDifference + '%';
};

var sendTextMessage = function( message, apiData ) {
	var data = {
		User: apiData.username,
		Password: apiData.password,
		Message: message,
		PhoneNumbers: apiData.phoneNumber
	};

	var options = {
		url: apiData.endpoint,
		method: 'POST',
		form: data,
		qs: {
			format: 'json'
		}
	};

	makeRequest( options ).then( function( response ) {
		var data = response ? JSON.parse( response ) : '';

		if ( data.Response.Code === 201 ) {
			saveToLog( 'Sent to ' + JSON.stringify( data.Response.Entry.PhoneNumbers ), 'Text Message' );
			lastNotificationTimestamp = moment().format( 'X' );
		} else {
			saveToLog( JSON.stringify( data.Response ), 'Text Message' );
		}
	});

};

var refreshExchangeRate = function( options ) {
	var endpoint = options.exchangeRateURL + options.exchangeType;
	var historicalTimestamp = moment().subtract( options.changeTimeDifference, 'hours' ).format( 'X' );

	var promises = {
		currentPrice: getPrice( { url: endpoint } ),
		historicalPrice: getPrice( { url: endpoint, qs: { timestamp: historicalTimestamp } } )
	};

	rsvp.hash( promises ).then( function( results ) {
		var values = calculateValues( results.currentPrice, results.historicalPrice );
		var message = generateMessage( values, options.exchangeType, options.changeTimeDifference );

		if ( isConditionMet( values, options.triggerPercentage ) ) {
			sendTextMessage( message, smsData );
		};

		console.log( 'Current Price:', values.currentPrice, '\t', options.changeTimeDifference + 'hours ago:', values.historicalPrice, '\t', 'Change:', values.percentageDifference + '%' );
	});

};

// Checks to see if notifications should be sent.
// Should not notify if the set notification count has reached
var isNotified = function( lastNotification ) {
	var notificationPeriod = 24;

	if ( moment().diff( moment( lastNotification, 'X') , 'hours' ) < notificationPeriod ) {
		return true;
	}

	return false;
};

setInterval( function() {

	if (! isNotified( lastNotificationTimestamp ) ) {
		refreshExchangeRate( settings );
	}

}, ( settings.updateInterval * 60000 ) );